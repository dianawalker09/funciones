<?php 
	class Empleado{
		private $nombre;
		private $horasTrabajadas;
		private $sueldoNominal;
		private $sueldoLiquido;
		
		private $valorDescuentoAFP=0.03;
		private $valorDescuentoISS=0.04;
		private $montoDescuentoAFP;
		private $montoDescuentoISS;
		private $valorHoraNormal=7;
		private $valorHoraExtra=10;
		
		private $horasSemanalNormal=40;
		
		function __construct($nombre, $horasTrabajadas){
			$this->nombre=$nombre;
			$this->horasTrabajadas=$horasTrabajadas;
			
			$this->calcularSueldoNominal();
			$this->calcularDescuentoAfp();
			$this->calcularDescuentoIss();
			$this->calcularSueldoLiquido();
			
		}
		
		function calcularSueldoNominal(){
			if($this->horasTrabajadas<=$this->horasSemanalNormal)
			{
				$this->sueldoNominal=$this->horasTrabajadas*$this->valorHoraNormal;
			}
			else{
				$horasMas=$this->horasTrabajadas-$this->horasSemanalNormal;
				$this->sueldoNominal=($this->horasTrabajadas-$horasMas)*$this->valorHoraNormal;
				$this->sueldoNominal+=$horasMas*$this->valorHoraExtra;
			}
			
		}
		
		function calcularDescuentoAfp(){
			$this->montoDescuentoAFP=$this->sueldoNominal*$this->valorDescuentoAFP;
		}
		function calcularDescuentoIss(){
			$this->montoDescuentoISS=$this->sueldoNominal*$this->valorDescuentoISS;
		}
		
		function calcularSueldoLiquido(){
			$this->sueldoLiquido=$this->sueldoNominal-$this->montoDescuentoAFP-$this->montoDescuentoISS;
		}
		
		function imprimirBoleta(){
			echo $this->nombre."<br>";
			echo "Salario nominal: ".$this->sueldoNominal."<br>";
			echo "Descuentos"."<br>";
			echo "Descuento AFP"."<br>";
			echo $this->montoDescuentoAFP."<br>";
			echo "Descuento ISS<br>";
			echo $this->montoDescuentoISS."<br>";
			echo "Descuento Total: ".($this->montoDescuentoISS+$this->montoDescuentoAFP)."<br>";
			
			echo "Sueldo liquido: " .$this->sueldoNominal. "-" . ($this->montoDescuentoISS+$this->montoDescuentoAFP)
			."<br>";
			echo "Sueldo liquido: ". $this->sueldoLiquido."<br>";
			
		}
		
	}
	
	$pedro=new Empleado("Juan Perez","50");
	$pedro->imprimirBoleta();
?>