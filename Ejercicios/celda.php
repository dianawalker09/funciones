<?php 
    class claseCelda {
		//Propiedad de las celdas
		  private $colorFondo;
		  private $colorLetra;
		  private $tipoLetra;
		  private $tamanoLetra;
		   
		 //Mètodo constructor de la clase claseCelda
		 function __construct($bgcolor, $fgcolor, $$forntfam, $fontsize) {
			 $this->colorFondo = $bgcolor;
			 $this->colorLetra = $fgcolor;
			 $this->tipoLetra = $fontfam;
			 $this->tamanoLetra = $fontsize;
			 
			 //Metodos para configurar la celda 
			 function pinta_celda($contenidio) {
				 $celda ="<td style=\"font-family: {´$this->tipoLetra};";
				 $celda .= "font-size: {$this->tamanoLetra};";
				 $celda .="padding:12px 16px;";
				 $celda .="background: {$this->colorFondo};";
				 $celda .="color: {$this->colorLetra};\">\n";
				 $celda .= utf8_decode($contenidio) . "\n";
				 $celda .= "</td>\n";
				 echo $celda;
			 }
		 }
		     if(isset($_POST['enviar'])) {
			   $content = ($_POST['content']) ? $_POST['content'] : "";
			     if($content == "") {
					 $msg= "<h3>Debe escribir un nombre</h3>";
					 $msg .= "<a href=\"objetos.html\">Regresar al formulario</a>";
					 die($msg);
				 }
				 
				  $bgcolor = isset($_POST['bgcolor']) ? $_POST['bgcolor'] : "";
				  $bgcolor = isset($_POST['fgcolor']) ? $_POST['fgcolor'] : "";
				  $bgcolor = isset($_POST['font']) ? $_POST['font'] : "";
				  $bgcolor = isset($_POST['size'])  $_POST['size'] : "";
				  $nuevacelda = new claseCelda($bgcolor, $fgcolor, $fontfam, $fontsize);
				  echo "<table>\n<tr>\n";
				  $nuevacelda->pinta_celda($content);
				  echo "<\tr>\n</table>\n";
			
?>