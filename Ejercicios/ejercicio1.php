html lang="es"
  <head>
    <title>Configuracion</title>
 </head>
 <body>
    <section>
	   <article>
	        <form action="celda.php" method="POST">
			      <legend><span>Configuracion de Celdas</span></legend>
				 <ul>
				    <li>
					   <div class="campo">
					        <label for="content">Nombre Completo:</label>
							<input type="text" name="content" size="40" maxlength="250" />
						</div>
					</li>
				    <li>
					    <div class="campo">
						     <label for="bgcolor">Color de fondo:</label>
							 <select name="bgcolor">
							     <option value="Blue" selected="selected">Azul</option>
								 <option value="DarkGreen">Verde</option>
								 <option value="Red">Rojo</option>
								 <option value="Orange">Naranja</option>
								 <option value="Brown">Cafe</option>
							</selected>
							</div>
							</li>
							 <div class="campo">
							    <label for="front">Fuente:</label>
								<select name="front">
								<option value="Arial" selected="selected">Arial</option>
								 <option value="Verdana">Verdana</option>
								  <option value="Helvetica">Helvetica</option>
								</select>
							</div>
						</li>
						   <li>
						      <div class="campo">
							  <label for="size">Tamano:</label>
							  <select name="size">
							    <option value="16px" selected="selected">16</option>
								<option value="18px">18</option>
								<option value="20px">20</option>
								<option value="30px">30</option>
							</select>
							</div>
						</li>
						<li>
						
								<div class="boton">
								 <input type="submit" name="enviar" id="enviar" value="Enviar" />
								 </div>
								</li>
			                   </ul>
							   
			 </form>
	</article>
	</section>
	</body>
</html>